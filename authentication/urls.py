from django.urls import path
from .views import RegisterView
from rest_framework_simplejwt import views as jwt_views

urlpatterns = [
    path('register/', RegisterView.as_view(), name="register"),
    path('token/', jwt_views.TokenObtainPairView.as_view(), name="get_token"),
    path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name="refresh_token"),
]