from rest_framework.pagination import PageNumberPagination
from rest_framework.throttling import UserRateThrottle, ScopedRateThrottle
from django.conf import settings
from simpleAPI.models import Snippet
from simpleAPI.serializers.model_serializers import SnippetModelSerializer
from django.http import Http404, HttpResponseRedirect
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework import authentication
from simpleAPI.permissions import IsOwnerOrReadOnly
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS
from simpleAPI.permissions import ReadOnly

# class LargeResultsSetPagination(PageNumberPagination):
#     page_size = 1
#     page_size_query_param = 'page_size'
#     max_page_size = 1


class SnippetList(APIView):
    """
    List all snippets, or create a new snippet.
    """
    # authentication_classes = [authentication.BasicAuthentication]
    # permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    # throttle_classes = [ScopedRateThrottle]
    # throttle_scope = 'snippet_list_2'

    def get(self, request, format=None):
        snippets = Snippet.objects.all()
        serializer = SnippetModelSerializer(snippets, many=True)
        return Response(serializer.data, headers={'test':'test'})

    def post(self, request, format=None):
        serializer = SnippetModelSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(owner=self.request.user)
            return Response(data=serializer.initial_data, status=status.HTTP_201_CREATED)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    # def handle_exception(self, exc):
    #     print(exc)


class SnippetDetail(APIView):
    permission_classes = [ReadOnly]

    def get_object(self, id):
        try:
            return Snippet.objects.get(pk=id)
        except Snippet.DoesNotExist:
            raise Http404

    def get(self, request, id, format=None):
        snippet = self.get_object(id)
        serializer = SnippetModelSerializer(instance=snippet)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id, format=None):
        snippet = self.get_object(id)
        serializer = SnippetModelSerializer(instance=snippet, data=request.data)
        self.check_object_permissions(request=request, obj=snippet)
        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=status.HTTP_200_OK)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, format=None):
        snippet = self.get_object(id)
        try:
            self.check_object_permissions(request=request, obj=snippet)
            snippet.delete()
            return Response(data={'status': 'oke'}, status=status.HTTP_200_OK)
        except:
            return Response(data={'status': 'fail'}, status=status.HTTP_400_BAD_REQUEST)








