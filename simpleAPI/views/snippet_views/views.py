from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from simpleAPI.models import Snippet
from simpleAPI.serializers.model_serializers import SnippetModelSerializer


@csrf_exempt
def snippet_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method =='GET':
        snippets = Snippet.objects.all()
        serializer = SnippetModelSerializer(snippets, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method =='POST':
        data = JSONParser().parse(request)
        serializer = SnippetModelSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


@csrf_exempt
def snippet_detail(request, id):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        snippet = Snippet.objects.get(pk=id)
    except Snippet.DoesNotExist:
        return JsonResponse(status=404)

    if request.method =='GET':
        serializer = SnippetModelSerializer(snippet)
        return JsonResponse(data=serializer.data)

    elif request.method == "PUT":
        data = JSONParser().parse(request)
        serializer = SnippetModelSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(data=serializer.data, status=200)
        return JsonResponse(status=404)

    elif request.method == 'DELETE':
        snippet.delete()
        return JsonResponse(data={'status': 'success'}, status=200)


