from rest_framework import serializers
from simpleAPI.models import Snippet, LANGUAGE_CHOICES, STYLE_CHOICES
from authentication.models import User
from rest_framework import serializers


serializer = serializers.Serializer()

'''
serializer use just for decode and encode object to json and reverse
'''
class SnippetModelSerializer(serializers.ModelSerializer):
    owner = serializers.CharField(source='owner.email', read_only=True, required=False)
    # owner = serializers.StringRelatedField(many='true')
    # owner = serializers.PrimaryKeyRelatedField(read_only=True)
    # owner = serializers.HyperlinkedRelatedField()

    class Meta:
        model = Snippet
        fields = ['id', 'title', 'code', 'owner']


class UserModelSerializer(serializers.ModelSerializer):
    # snippets = serializers.PrimaryKeyRelatedField(many=True, queryset=Snippet.objects.all())
    # snippets = SnippetModelSerializer(read_only=True, many=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'is_staff', 'is_superuser', 'snippets']

    def create(self, validated_data):
        snippets_data = validated_data['snippets']
        user = User.objects.create(**validated_data)




