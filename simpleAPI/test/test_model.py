import django
django.setup()
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from simpleAPI.models import Snippet
from dotenv import load_dotenv
import os
import requests


load_dotenv()
os.environ['DJANGO_SETTINGS_MODULE'] = 'tutorial.settings'

class AccountTests(APITestCase):
    def test_create_account(self):
        """
        Ensure we can create a new account object.
        """
        url = 'http://127.0.0.1:8001/snippets/'


        res = requests.get(url=url)

        assert res.status_code == 200
