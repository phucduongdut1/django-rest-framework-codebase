from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from simpleAPI.views.snippet_views import class_base_view
from simpleAPI.views.snippet_views import function_base_views

urlpatterns = [
    path('', class_base_view.SnippetList.as_view()),
    path('<int:id>/', class_base_view.SnippetDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)