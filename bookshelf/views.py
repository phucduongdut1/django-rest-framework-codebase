from django.contrib.auth.models import AnonymousUser
from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import BookShelf
from .serializers import BookShelfSerializer


class BookShelfList(APIView):
    def get(self, request):
        bookshelfs = BookShelf.objects.all()
        serializer = BookShelfSerializer(bookshelfs, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = BookShelfSerializer(data=request.data)

        if serializer.is_valid():
            if not isinstance(request.user, AnonymousUser):
                serializer.save(owner=request.user)

            serializer.save()
            return Response(data=serializer.validated_data, status=status.HTTP_201_CREATED)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
