from django.db import models
from authentication.models import User


# Create your models here.
class BookShelf(models.Model):
    name = models.CharField(max_length=100, blank=False, unique=True)

    owner = models.ForeignKey(User, related_name='book_shelf', on_delete=models.CASCADE, blank=True, null=True)