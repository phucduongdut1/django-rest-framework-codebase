from django.urls import path
from .views import BookShelfList


urlpatterns = [
    path('', BookShelfList.as_view())
]