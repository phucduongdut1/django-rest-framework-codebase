from rest_framework import serializers
from .models import BookShelf


class BookShelfSerializer(serializers.ModelSerializer):
    owner = serializers.CharField(source='owner.email', read_only=False, required=False, allow_blank=True)

    class Meta:
        model = BookShelf
        fields = ['name', 'owner']

    # def create(self, validated_data):
    #     return BookShelf(**validated_data)