from django.db import models
from bookshelf.models import BookShelf


class Book(models.Model):
    name = models.CharField(max_length=200, blank=False)
    author = models.CharField(max_length=100, blank=True)

    bookshelf = models.ForeignKey(BookShelf, related_name='books', on_delete=models.CASCADE, blank=True, null=True)


