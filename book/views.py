from django.shortcuts import render
from rest_framework.views import APIView
from .models import Book
from .serializers import BookSerializer
from rest_framework.response import Response
from rest_framework import status
from bookshelf.models import BookShelf
from rest_framework.response import Response
from django.conf import settings
from rest_framework import permissions, authentication


class BookList(APIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get(self, request):
        books = Book.objects.all()
        serializer = BookSerializer(books, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = BookSerializer(data=request.data)
        try:
            bookshelf = BookShelf.objects.get(name=request.data.get('bookshelf', None))
        except:
            bookshelf = None

        if serializer.is_valid():
            serializer.save(bookshelf=bookshelf)
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)



