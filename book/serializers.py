from rest_framework import serializers
from book.models import Book


class BookSerializer(serializers.ModelSerializer):
    bookshelf = serializers.CharField(source='bookshelf.name', max_length=200, allow_blank=True, required=False)

    class Meta:
        model = Book
        fields = ['name', 'author', 'bookshelf']

    def create(self, validated_data):
        return Book(**validated_data)